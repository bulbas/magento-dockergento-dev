Environment for deploying a local Magento server basen on [ModestCoders/magento2-dockergento](https://github.com/ModestCoders/magento2-dockergento) with additional tools:

* sendmail with [mailhog](https://github.com/mailhog/MailHog)
* phpmyadmin
* [elastichq](https://github.com/ElasticHQ/elasticsearch-HQ)

and you can use ssl certificates

**tools are available at the following addresses:**

* mailhog - http://localhost:8025
* phpmyadmin - http://localhost:8080
* elastichq http://localhost:5000

## Usage ##

1. init dockergento project

2. Сopy the files from this repository to the Magento project folder. Replace docker-compose.yml

### Generating certificates for HTTPS ###

use mkcert - [FiloSottile/mkcert](https://github.com/FiloSottile/mkcert)

**Install certutil**

```bash
sudo apt install libnss3-tools
```

**Install Homebrew**

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

**Install mkcert**

```bash
brew install mkcert
```

**make certificate**

```bash
mkcert -install
mkcert example.com "*.example.com" example.test localhost 127.0.0.1 ::1
```

copy ".pem" and "-key.pem" fiels to <magento_project_dir>/config/dockergento/nginx/cert

### Change Nginx config ###

edit <magento_project_dir>/config/dockergento/nginx/conf file


change listen port

```
#  listen 8000;
listen 4430;
```

add certificates

```
ssl on;
ssl_certificate /var/www/cert/<your pem file>;
ssl_certificate_key /var/www/cert/<your key.pem file>;
```

and at the end of the file add the code:

```
server {
		listen 8000;
		server_name <YOUR SERVER NAME, example.local>;
		location / {
				return 301 https://<YOUR SERVER NAME, example.local>$request_uri;
		}
 }
```
